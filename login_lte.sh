#!/bin/sh
LOCKFILE="/tmp/login_lte.pid"
TMP=$(mktemp -u)
WEBADDR="192.168.0.1"
PASSWORD="cGFzc3dvcmQ=" # in base64

# GENERAL FUNCTIONS
operation_prepare() {
	touch ${TMP}
	curl 'http://'${WEBADDR}'/goform/goform_set_cmd_process' -q -c ${TMP} -b ${TMP} -H 'Referer: http://'${WEBADDR}'/index.html' --data 'isTest=false&goformId=LOGIN&password='${PASSWORD}
	echo
}
operation_cleanup() {
	echo
	rm ${TMP}
}

# OPERATIONS
connect_network() {
	operation_prepare
	curl 'http://'${WEBADDR}'/goform/goform_set_cmd_process' -q -c ${TMP} -b ${TMP} -H 'Referer: http://'${WEBADDR}'/index.html' --data 'isTest=false&notCallback=true&goformId=CONNECT_NETWORK'
	operation_cleanup
}
disconnect_network() {
	operation_prepare
	curl 'http://'${WEBADDR}'/goform/goform_set_cmd_process' -q -c ${TMP} -b ${TMP} -H 'Referer: http://'${WEBADDR}'/index.html' --data 'isTest=false&notCallback=true&goformId=DISCONNECT_NETWORK'
	operation_cleanup
}
check_status() {
	curl -s 'http://'${WEBADDR}'/goform/goform_get_cmd_process?isTest=false&cmd=ppp_status' | grep 'disconnected' > /dev/null
	if [ $? -eq 0 ]; then
		echo MiFi network disconnected
		return 0
	fi
	echo MiFi network connected
	return 1
}

# SUBCOMMANDS
daemon () {
	if [ -e $LOCKFILE ]; then
		ps w | grep -v grep | grep $(cat $LOCKFILE) | grep $0 > /dev/null
		if [ $? -eq 0 ]; then
			echo "Another script is running, exiting..."
			exit 233
		fi
	fi

	echo $$ > $LOCKFILE

	while :; do
		check_status
		if [ $? -eq 0 ]; then
			echo MiFi network is going to reconnect
			connect_network
		fi
		sleep 3
	done
}

help() {
	cat <<EOF
Commands:
	daemon
	connect_network
	disconnect_network
	check_status
EOF
}

ACTION=${1:-help}
$ACTION "$@"